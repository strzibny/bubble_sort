/*
* Ruby C extension
*/

#include <ruby.h>    /* to extend Ruby */

/* TODO:
osetreni vstupu array s cisly
*/

static VALUE cbubble_sort(VALUE self, VALUE array)
{
  Check_Type(array, T_ARRAY);

  //rb_raise(rb_eTypeError, "sort() can only compare integers");
  //break;

  int lenght;
  VALUE *ary;
  /* Ruby 1.8 incompatible! include pitfalls */
  ary = RARRAY_PTR(array); /* convert Ruby array to C array */
  lenght = RARRAY_LEN(array); /* we need the lenght of the array */

  int element;
  for (element = 0; element < lenght; element++)
  {
    //Check_Type(ary[element], T_FIXNUM);
    if(TYPE(ary[element]) != T_FIXNUM){
      rb_raise(rb_eTypeError, "sort() can only compare integers");
    }
  }

  int c, d, swap;
  for (c = 0; c < ( lenght - 1 ); c++)
  {
    for (d = 0; d < lenght - c - 1; d++)
    {
      if (ary[d] > ary[d+1])
      {
        swap      = ary[d];
        ary[d]   = ary[d+1];
        ary[d+1] = swap;
      }
    }
  }

  array = rb_ary_new_from_values(lenght, ary);
  return array;
}

void Init_bubble_sort(void) {
  VALUE CBubbleSort = rb_define_module("BubbleSort");
  rb_define_singleton_method(CBubbleSort, "sort", cbubble_sort, 1); /* one argument */
}
