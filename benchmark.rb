require 'benchmark/ips'
require 'bubble_sort'

Benchmark.ips do |x|
  x.report("Ruby version") {

    a = [3, 55, 54, 2, 1, 0]

    1.times do
      BubbleSortRuby.sort a
    end
  }

  x.report("C version") {
    #require 'bubble_sort/bubble_sort'
    a = [3, 55, 54, 2, 1, 0]

    1.times do
      BubbleSort.sort a
    end
  }

  x.compare!
end
